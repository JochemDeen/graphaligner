#include "functions.h"

void findMaxScore(scoringStruct& scores,double& thisscore,
  double& holdpenalty,double& skippenalty){

  double stepscore = scores.stepscore + thisscore;
  double holdscore = scores.holdscore + holdpenalty;
  double skipscore = scores.skipscore + skippenalty;

  scores.maxscore = stepscore;
  scores.step = 1;

  if (holdscore>scores.maxscore){
    scores.maxscore = holdscore;
    scores.step = 2;
  }
  if (skipscore>scores.maxscore){
    scores.maxscore = skipscore;
    scores.step = 3;
  }
}

double mean(vector<double> scores){
  return std::accumulate(scores.begin(), scores.end(), 0.0) / scores.size();
}

double MaxScore(vector<double> scores, int& max_i){
  double maxscore = scores[0];
  max_i = 0;
  for(int i=0; i<scores.size(); i++){
    if (scores[i]>maxscore){
      maxscore = scores[i];
      max_i = i;
    }
  }
  return maxscore;
}

double MaxScore(vector<vector<double>> scores, int& max_i, int& max_j){
  double maxscore = scores[0][0];
  max_i = 0;
  max_j = 0;
  for(int i = 0; i<scores.size(); i++){
    for(int j = 0; j<scores[i].size(); j++){
      if (scores[i][j]>maxscore){
        maxscore = scores[i][j];
        max_i = i;
        max_j = j;
      }
    }
  }
  return maxscore;
}
