//#include "mex.h"
#include "mex.hpp"
#include "mexAdapter.hpp"

#include <vector>
using std::vector;
#include <assert.h>
#include <string>
#include <memory>
#include <algorithm>    // std::max

#include "Graph.h"
#include "alignment.h"

#include <boost/algorithm/string.hpp>


using namespace matlab::mex;
using namespace matlab::data;

//void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
