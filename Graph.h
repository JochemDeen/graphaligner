#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED
#endif // GRAPH_H_INCLUDED

#include <vector>

using std::vector;


class Node;

class Node{
  vector<Node*> inNodes;
  vector<Node*> outNodes;
  vector<int> weights;

  public:
  Node();//default Constructor
  Node(int thisID, vector<int> ivalues);
  Node(int thisID, vector<double> ivalues);
  int ID;

  vector<int> intensityvaluesRounded;
  vector<double> intensityvalues;
  int frequencies;

  void AddInNode(Node& node);
  void AddOutNode(Node& node);
  int OutNodesSize();
  int InNodesSize();

  void SwitchEdges();

  vector<Node*> GetInNodes();
  vector<Node*> GetOutNodes();
  Node* GetInNodes(int i);
  Node* GetOutNodes(int i);

};



class Graph{
public:
  Graph(); //default constructor
  Graph(vector<vector<int>> nodeMatrix,vector<vector<int>> intensityvalues);
  Graph(vector<vector<int>> nodeMatrix,vector<vector<double>> intensityvalues);

  vector<vector<double>> Lookuptable;
  vector<Node> nodeList;

  int getsize();
  Node GetNode(int i);
  void InvertGraph();
  void LinkNodes(vector<vector<int>> nodeMatrix);
  void MakeNodes(vector<vector<int>> intensityvalues);
  void MakeNodes(vector<vector<double>> intensityvalues);

};
