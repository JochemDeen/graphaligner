#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED
#endif // FUNCTIONS_H_INCLUDED

#ifndef ALIGNMENT_H_INCLUDED
#include "alignment.h"
#endif // ALIGNMENT_H_INCLUDED

#include <numeric>

struct scoringStruct;

const int NANI = -1;

void findMaxScore(scoringStruct& scores,double& thisscore,double& holdpenalty,double& skippenalty);
double MaxScore(vector<double> scores, int& max_i);
double MaxScore(vector<vector<double>> scores, int& max_i, int& maxj);
double mean(vector<double> scores);
