#ifndef GRAPH_H_INCLUDED
#include "Graph.h"
#endif // GRAPH_H_INCLUDED

using namespace std;

Node::Node(){}//default Constructor

Node::Node(int thisID, vector<int> ivalues){
  ID = thisID;
  intensityvaluesRounded = ivalues;
}

Node::Node(int thisID, vector<double> ivalues){
  ID = thisID;
  intensityvalues = ivalues;
  for (int i=0;i<ivalues.size();i++){
    double x = ivalues[i] + 0.5 - (ivalues[i]<0);
    intensityvaluesRounded.push_back((int)x);
  }
}

void Node::AddInNode(Node& inNode){
  inNodes.push_back(&inNode);
}

void Node::AddOutNode(Node& outNode){
  outNodes.push_back(&outNode);
}

int Node::OutNodesSize(){
  return outNodes.size();
}

int Node::InNodesSize(){
  return inNodes.size();
}

void Node::SwitchEdges(){
  vector<Node*> tempInNodes;
  vector<Node*> tempOutNodes;

  for(int i=0;i<inNodes.size();i++){
    tempOutNodes.push_back(inNodes[i]);
  }

  for(int j=0;j<outNodes.size();j++){
    tempInNodes.push_back(outNodes[j]);
  }
  inNodes = tempInNodes;
  outNodes = tempOutNodes;
}

vector<Node*> Node::GetInNodes(){
  return inNodes;
}

Node* Node::GetInNodes(int i){
  return inNodes[i];
}

Node* Node::GetOutNodes(int i){
  return outNodes[i];
}

vector<Node*> Node::GetOutNodes(){
  return outNodes;
}

Graph::Graph(){} //default constructor

Graph::Graph(vector<vector<int>> nodeMatrix,vector<vector<int>> intensityvalues){
  MakeNodes(intensityvalues);
  LinkNodes(nodeMatrix);
}

Graph::Graph(vector<vector<int>> nodeMatrix,vector<vector<double>> intensityvalues){
  MakeNodes(intensityvalues);
  LinkNodes(nodeMatrix);
}

void Graph::MakeNodes(vector<vector<double>> intensityvalues){
  int nodes = intensityvalues.size();

  nodeList.clear();

  for(int i=0;i<nodes;i++){
    Node thisNode(i, intensityvalues[i]);
    nodeList.push_back(thisNode);
  }
}

void Graph::MakeNodes(vector<vector<int>> intensityvalues){
  int nodes = intensityvalues.size();
  nodeList.clear();

  for(int i=0;i<nodes;i++){
    Node thisNode(i, intensityvalues[i]);
    nodeList.push_back(thisNode);
  }
}

void Graph::LinkNodes(vector<vector<int>> nodeMatrix){
  int nodes = nodeMatrix.size();

  for(int i=0;i<nodes;i++){
    for(int j=0;j<nodes;j++){
      if (nodeMatrix[i][j]>0){
        Node* node1 = &nodeList[i];
        Node* node2 = &nodeList[j];
        //Edge edge(*node1,*node2,nodeMatrix[i][j]);

        node2->AddInNode(*node1);
        node1->AddOutNode(*node2);
      }
    }
  }

}

int Graph::getsize(){
  return nodeList.size();
}

Node Graph::GetNode(int i){
  return nodeList[i];
}

void Graph::InvertGraph(){
  for(int i=0;i<nodeList.size();i++){
    nodeList[i].SwitchEdges();
  }
}
