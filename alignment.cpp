#ifndef ALIGNMENT_H_INCLUDED
#include "alignment.h"
#endif // ALIGNMENT_H_INCLUDED


gg_alignment::gg_alignment(){}//default constructor

gg_alignment::gg_alignment(Graph Input1, Graph Input2){
  Graph1 = Input1;
  Graph2 = Input2;

  int n = Graph1.getsize();
  int m = Graph2.getsize();

  alignment_initialize(n,m);
};

gg_alignment::gg_alignment(Graph Input1, Graph Input2, bool rounded){
  Rounded = rounded;
  Graph1 = Input1;
  Graph2 = Input2;

  int n = Graph1.getsize();
  int m = Graph2.getsize();

  alignment_initialize(n,m);
};

gg_alignment::gg_alignment(Graph Input1, Graph Input2, vector<vector<double>> lutable){
  lookuptable = lutable;
  Graph1 = Input1;
  Graph2 = Input2;
  int n = Graph1.getsize();
  int m = Graph2.getsize();

  alignment_initialize(n,m);
}

gg_alignment::gg_alignment(Graph Input1, Graph Input2, double sigma){
  Rounded = false;
  Sigma = sigma;
  Graph1 = Input1;
  Graph2 = Input2;
  int n = Graph1.getsize();
  int m = Graph2.getsize();

  alignment_initialize(n,m);
}

gg_alignment::gg_alignment(Graph Input1, Graph Input2, vector<vector<double>> lutable,double pen1, double pen2){
  Graph1 = Input1;
  Graph2 = Input2;
  int n = Graph1.getsize();
  int m = Graph2.getsize();

  addParameters(lutable, pen1, pen2);
  alignment_initialize(n,m);
}

gg_alignment::gg_alignment(Graph Input1, Graph Input2, double sigma,double pen1, double pen2){
  Rounded = false;
  Graph1 = Input1;
  Graph2 = Input2;
  int n = Graph1.getsize();
  int m = Graph2.getsize();

  addParameters(sigma, pen1, pen2);
  alignment_initialize(n,m);
}

void gg_alignment::addParameters(double sigma, double pen1, double pen2){
  holdpenalty = pen1;
  skippenalty = pen2;
  Sigma = sigma;
}

void gg_alignment::addParameters(vector<vector<double>> lutable, double pen1, double pen2){
  holdpenalty = pen1;
  skippenalty = pen2;
  lookuptable = lutable;
}

void gg_alignment::alignment_initialize(int n, int m){
  //initialize
  for(int i=0; i<n; i++){
    vector<double> z;
    vector<int> y;
    vector<bool> x;
    for(int j=0; j<m; j++){
    z.push_back(NAN);
    y.push_back(NANI);
    x.push_back(false);
    }
    alignmentscores.push_back(z);
    step.push_back(y);
    branch1.push_back(y);
    branch2.push_back(y);
    alignment_bool.push_back(x);
  }
}

void gg_alignment::RunAlignment(){
  int n = Graph1.getsize();
  int m = Graph2.getsize();

  for(int i=0; i<n; i++){
    Node node1 = Graph1.GetNode(i);
    for(int j=0; j<m; j++){
      Node node2 = Graph2.GetNode(j);
      AlignTwoNodes(node1,node2);
    }
  }
};

void gg_alignment::AlignTwoNodes(Node& node1,Node& node2){
   //Calculate current ScoreExact
   double thisscore;
   if (Rounded){
     thisscore = CalculateScore(node1, node2);
   } else{
     thisscore = CalculateScoreExact(node1, node2);
   }

    //Get best old scores
    scoringStruct scores;
    GetBestOldScores(node1,node2, scores);

    //Get max score
    findMaxScore(scores,thisscore,holdpenalty,skippenalty);

    //Fill in alignent matrix
    FillInMatrix(scores, node1, node2);

    //Set up maxScore
    if (node1.OutNodesSize() == 0 || node2.OutNodesSize() == 0){
      if (scores.maxscore>maxEndScore){
        maxEndScore = scores.maxscore;
        endnode1 = node1;
        endnode2 = node2;
      }
    }
};

double gg_alignment::CalculateScore(Node& node1, Node& node2){

  vector<double> scores;
  vector<int> int1vals = node1.intensityvaluesRounded;
  vector<int> int2vals = node2.intensityvaluesRounded;

  for(int i=0;i<int1vals.size();i++){
    for(int j=0;j<int2vals.size();j++){
       double thisscore = Score(int1vals[i],int2vals[j]);
       scores.push_back(thisscore);
     }
   }
  double score = mean(scores);
  score = log(score)+2.7;
  return score;
};

double gg_alignment::CalculateScoreExact(Node& node1, Node& node2){

  vector<double> scores;
  vector<double> int1vals = node1.intensityvalues;
  vector<double> int2vals = node2.intensityvalues;

  for(int i=0;i<int1vals.size();i++){
    for(int j=0;j<int2vals.size();j++){
       double thisscore = ScoreExact(int1vals[i],int2vals[j]);
       scores.push_back(thisscore);
     }
   }
  double score = mean(scores);
  return score;
};

double gg_alignment::Score(int val1, int val2){
  assert(val1<=lookuptable.size()-1);
  assert(val2<=lookuptable[val1].size()-1);
  double score = lookuptable[val1][val2];
  return score;
}

double gg_alignment::ScoreExact(double val1, double val2){
  double diff = (val1-val2)*(val1-val2);
  double score = 1 - diff/(2*Sigma*Sigma);
  return score;
}

void gg_alignment::GetBestOldScores(Node& node1, Node& node2, scoringStruct& scores){
  vector<Node*> node1In = node1.GetInNodes();
  vector<Node*> node2In = node2.GetInNodes();

 //Hold node1
  if(node2In.size() == 0){
    scores.holdscore = 0;
    scores.holdidx = NANI;
  }else{
      vector<double> oldholdscores(node2In.size());
      for(int j=0; j<node2In.size(); j++) {
        Node* inNode = node2In[j];
        oldholdscores[j] = GetScoresforNode(node1,*inNode);
      }
      int max_i;
      double maxscore = MaxScore(oldholdscores, max_i);
      scores.holdscore = maxscore;
      scores.holdidx = max_i;
  }

  //Skip node1
  if (node1In.size() == 0){
    scores.skipscore = 0;
    scores.skipidx = NANI;
  } else{
    vector<double> oldskipscores(node1In.size());
    for(int j=0; j<node1In.size(); j++){
      Node* inNode = node1In[j];
      oldskipscores[j] = GetScoresforNode(*inNode,node2);
    }
    int max_i;
    double maxscore = MaxScore(oldskipscores, max_i);
    scores.skipscore = maxscore;
    scores.skipidx = max_i;
  }

  //step node1 and node2
  if ((node1In.size()>0) && (node2In.size()>0)) {
    vector<vector<double>> oldstepscores(node1In.size(),vector<double>(node2In.size(),1));
    for(int i=0;i<node1In.size();i++){
      //vector<double> rowscore(node2In.size());
      for(int j=0; j<node2In.size(); j++){
        Node* inNode1 = node1In[i];
        Node* inNode2 = node2In[j];
        oldstepscores[i][j] = GetScoresforNode(*inNode1,*inNode2);
      }
      //oldstepscores.push_back(rowscore);
    }
    int max_i;
    int max_j;
    double maxscore = MaxScore(oldstepscores, max_i, max_j);
    scores.stepscore = maxscore;
    scores.stepidx.push_back(max_i);
    scores.stepidx.push_back(max_j);
    } else {
      scores.stepscore = 0;
      scores.stepidx.push_back(NANI);

  };
};

double gg_alignment::GetScoresforNode(Node& node1, Node& node2){
  int i = node1.ID;
  int j = node2.ID;

  if (!alignment_bool[i][j]){ //isNAN(alignmentscores[i][j]){
    AlignTwoNodes(node1,node2);
  }
  double score = alignmentscores[i][j];
  return score;
};

void gg_alignment::FillInMatrix(scoringStruct scores, Node &node1, Node &node2){
  int i = node1.ID;
  int j = node2.ID;

  alignmentscores[i][j] = scores.maxscore;
  step[i][j] = scores.step;
  alignment_bool[i][j] = true;

  switch (scores.step){
    case 1 : { //step
      branch1[i][j] = scores.stepidx[0]; //branch1;
      branch2[i][j] = scores.stepidx[1];
    } break;
    case 2 : //hold
      branch2[i][j] = scores.holdidx;//branch2;
      break;
    case 3 : //skip
      branch1[i][j] = scores.skipidx;//branch1;
      break;
  };
};

void gg_alignment::StartBackTrace(){
  BackTrace(endnode1,endnode2);
  std::reverse(backTrace1.begin(), backTrace1.end());
  std::reverse(backTrace2.begin(), backTrace2.end());
  std::reverse(TraceScore.begin(), TraceScore.end());
}
void gg_alignment::BackTrace(Node& node1,Node& node2){
  int ID1 = node1.ID;
  int ID2 = node2.ID;
  backTrace1.push_back(ID1);
  backTrace2.push_back(ID2);
  TraceScore.push_back(alignmentscores[ID1][ID2]);

  if (node1.InNodesSize()>0 && node2.InNodesSize()>0){
     Node nextnode1;
     Node nextnode2;
     DetermineNextStep(node1, node2, nextnode1, nextnode2);
     BackTrace(nextnode1,nextnode2);
  }
}

void gg_alignment::DetermineNextStep(Node& node1, Node& node2, Node& nextnode1, Node& nextnode2){
  int Id1 = node1.ID;
  int Id2 = node2.ID;

  int thisstep = step[Id1][Id2];
  switch (thisstep){
    case 1 : { //step
      int branch1idx = branch1[Id1][Id2];
      nextnode1 = *node1.GetInNodes(branch1idx);
      int branch2idx = branch2[Id1][Id2];
      nextnode2 = *node2.GetInNodes(branch2idx);
    } break;
    case 2 : { //node
      nextnode1 = node1;
      int branch2idx = branch2[Id1][Id2];
      nextnode2 = *node2.GetInNodes(branch2idx);
    }  break;
    case 3 :{ //skip
      int branch1idx = branch1[Id1][Id2];
      nextnode1 = *node1.GetInNodes(branch1idx);
      nextnode2 = node2;
    } break;
  };
}
