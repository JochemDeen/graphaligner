#include "GraphAligner.h"

#define WITH_MATLAB

class MexFunction : public Function {
private:
  std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr;
  matlab::data::ArrayFactory factory;
public:
  /* Constructor for the class. */
  MexFunction()
  {
    matlabPtr = getEngine();
  }

  /* Helper function to generate an error message from given string,
   * and display it over MATLAB command prompt.
   */
  void displayError(std::string errorMessage)
  {
    ArrayFactory factory;
    matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
            0, std::vector<Array>({
      factory.createScalar(errorMessage) }));
  }

  /* Helper function to print output string on MATLAB command prompt. */
  void displayOnMATLAB(std::ostringstream stream)
  {
    ArrayFactory factory;
    matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("fprintf"),0, std::vector<Array>
            ({ factory.createScalar(stream.str())}));
  }

  void
  GetMatlabElements(matlab::data::StructArray matlabStructArray,matlab::data::Array matlabArray,
    vector<vector<int>> &nodeIntensityValues,vector<vector<int>> &graphMatrix){

    size_t total_num_of_elements = matlabStructArray.getNumberOfElements();
    for (size_t entryIndex=0; entryIndex<total_num_of_elements; entryIndex++) {
      Array const structField1 =
              matlabStructArray[entryIndex]["intensityvalues"];
        vector<int> intensityvalues;
        for(int i=0;i<structField1.getNumberOfElements();i++){
          intensityvalues.push_back(int(structField1[i]));
        }
        nodeIntensityValues.push_back(intensityvalues);
      }

      for(int i=0;i<matlabArray.getDimensions()[0];i++){
        vector<int> graphRow;
        for(int j=0;j<matlabArray.getDimensions()[1];j++){
            graphRow.push_back(matlabArray[i][j]);
      }
      graphMatrix.push_back(graphRow);
      }
  }

  void
  GetMatlabElements(matlab::data::StructArray matlabStructArray,matlab::data::Array matlabArray,
    vector<vector<double>> &nodeIntensityValues,vector<vector<int>> &graphMatrix){

    size_t total_num_of_elements = matlabStructArray.getNumberOfElements();
    for (size_t entryIndex=0; entryIndex<total_num_of_elements; entryIndex++) {
      Array const structField1 =
              matlabStructArray[entryIndex]["intensityvalues"];
        vector<double> intensityvalues;
        for(int i=0;i<structField1.getNumberOfElements();i++){
          intensityvalues.push_back(structField1[i]);
        }
        nodeIntensityValues.push_back(intensityvalues);
      }

      for(int i=0;i<matlabArray.getDimensions()[0];i++){
        vector<int> graphRow;
        for(int j=0;j<matlabArray.getDimensions()[1];j++){
            graphRow.push_back(matlabArray[i][j]);
      }
      graphMatrix.push_back(graphRow);
      }
  }

  void CheckInput(matlab::data::StructArray matlabStructArray,matlab::data::Array matlabArray){
    if (matlabStructArray.getType() != ArrayType::STRUCT) {
      displayError("Incorrect input, Graph needs to be a structure.");
    }

    auto fields = matlabStructArray.getFieldNames();
    vector<std::string> fieldNames(fields.begin(), fields.end());
    if(std::find(fieldNames.begin(), fieldNames.end(), "intensityvalues") == fieldNames.end()) {
      displayError("Incorrect input, Graph needs to contain 'intensityvalues'.");
    }

    if (matlabArray.getDimensions().size() != 2 || matlabArray.getDimensions()[0] != matlabArray.getDimensions()[1]) {
      displayError("Incorrect input, Node connections need to be a square matrix.");
    }

    if (matlabStructArray.getNumberOfElements() != matlabArray.getDimensions()[0]){
      displayError("Node connection matrix and Graph Structure are not the same size.");
    }

  }

  /* This is the gateway routine for the MEX-file. */
  void
  operator()(ArgumentList outputs, ArgumentList inputs) {
    //Checks
    if (inputs.size() < 6) {
      displayError("At least six input arguments are required:\n"
      "GraphAligner(method, Struct.Intensityvalues, NodeMatrix, Struct.Intensityvalues2, Nodematrix2, arg)\n"
      "with arg = Sigma or lookuptable");
    }

    //Check method for running alignment
    matlab::data::CharArray const array = inputs[0];
    std::string method = array.toAscii();
    bool rounded;
    if (boost::iequals(method,"rounded")) {
      rounded = true;
    } else if (boost::iequals(method,"exact")) {
      rounded = false;
    } else {
      displayError("Incorrect method.");
    }

    //Graph 1
    matlab::data::StructArray const matlabStructArray1 = inputs[1];
    matlab::data::Array const matlabArray1 = inputs[2]; //
    CheckInput(matlabStructArray1, matlabArray1);

    //Graph 2
    matlab::data::StructArray const matlabStructArray2 = inputs[3];
    matlab::data::Array const matlabArray2 = inputs[4]; //
    CheckInput(matlabStructArray2, matlabArray2);

    //Load Graph 1
    vector<vector<double>> nodeIntensityValues1;
    vector<vector<int>> graphMatrix1;
    GetMatlabElements(matlabStructArray1, matlabArray1, nodeIntensityValues1, graphMatrix1);

    //Make Graph1 structure
    Graph Graph1(graphMatrix1, nodeIntensityValues1);

    //Load Graph 2
    vector<vector<double>> nodeIntensityValues2;
    vector<vector<int>> graphMatrix2;
    GetMatlabElements(matlabStructArray2, matlabArray2, nodeIntensityValues2, graphMatrix2);

    //Make Graph2 structure
    Graph Graph2(graphMatrix2, nodeIntensityValues2);

    //Make Inverted Graph2
    Graph Graph2I(graphMatrix2, nodeIntensityValues2);
    Graph2I.InvertGraph();

    // Load lookuptable
    vector<vector<double>> lookuptable;
    double Sigma;
    if (rounded){
      matlab::data::Array const matlabArray3 = inputs[5]; //
      if (matlabArray3.getDimensions().size() != 2 ||
      matlabArray3.getDimensions()[0] != matlabArray3.getDimensions()[1]) {
        displayError("Incorrect input, lookuptable need to be a square matrix.");
      }
      for(int i=0;i<matlabArray3.getDimensions()[0];i++){
        vector<double> luRow;
        for(int j=0;j<matlabArray3.getDimensions()[1];j++){
            luRow.push_back(matlabArray3[i][j]);
      }
      lookuptable.push_back(luRow);
      };
    }else{
      matlab::data::Array const matlabArray3 = inputs[5]; //
      Sigma = inputs[5][0];
    }

    //Load penalties
    double holdpenalty;
    double skippenalty;
    double res=70;
    if (inputs.size() > 6){
      matlab::data::Array const matlabArray4 = inputs[6];
      if (matlabArray4.getNumberOfElements() == 1){
        double holdPercentage = matlabArray4[0];
        holdpenalty = log(holdPercentage/100)-log((100-2*holdPercentage)/100);
        skippenalty = log(holdPercentage/100)-log((100-2*holdPercentage)/100);
      }else{
        //matlab::data::Array const matlabArray4 = inputs[6]; //
        if (matlabArray4.getDimensions().size() > 2 || matlabArray4.getNumberOfElements()>2) {
          displayError("Incorrect input, penalties needs to be a 1 value or a 2-value matrix.");
        }
        holdpenalty = matlabArray4[0];
        skippenalty = matlabArray4[1];
      }
    }else{
      double holdPercentage = 15;
      holdpenalty = log(holdPercentage/100)-log(res/100);
      skippenalty = log(holdPercentage/100)-log(res/100);
    }
    std::ostringstream stream;

    //Initialize alignment
    gg_alignment Alignment(Graph1, Graph2, rounded);
    gg_alignment Alignment2(Graph1, Graph2I, rounded);
    if (rounded){
      Alignment.addParameters(lookuptable,holdpenalty,skippenalty);
      Alignment2.addParameters(lookuptable,holdpenalty,skippenalty);
    } else{
     Alignment.addParameters(Sigma,holdpenalty,skippenalty);
     Alignment2.addParameters(Sigma,holdpenalty,skippenalty);
    }
    Alignment.RunAlignment();
    Alignment2.RunAlignment();


    //Gather outputs
    double maxScore;
    int maxDirection;
    vector<int> backTrace1;
    vector<int> backTrace2;
    vector<double> TraceScore;
    vector<vector<double>> alignmentscores;

    if (Alignment.maxEndScore>Alignment2.maxEndScore){
      maxScore=Alignment.maxEndScore;
      maxDirection = 1;
      Alignment.StartBackTrace();
      backTrace1 = Alignment.backTrace1;
      backTrace2 = Alignment.backTrace2;
      TraceScore = Alignment.TraceScore;
      alignmentscores = Alignment.alignmentscores;
    }else{
      maxScore=Alignment2.maxEndScore;
      maxDirection = 2;
      Alignment2.StartBackTrace();
      backTrace1 = Alignment2.backTrace1;
      backTrace2 = Alignment2.backTrace2;
      TraceScore = Alignment2.TraceScore;
      alignmentscores = Alignment2.alignmentscores;
    }
    //Output 0 score
    outputs[0] = factory.createScalar(maxScore);

    //Output 1 Direction
    if (outputs.size()>1){
      outputs[1] = factory.createScalar(maxDirection);
    }

    // //Output 2 Backtrace
    matlab::data::Array outArray =
            factory.createArray<int>({backTrace1.size(),2});
    for(int i=0;i<backTrace1.size();i++){
      outArray[i][0] = backTrace1[i]+1;
      outArray[i][1] = backTrace2[i]+1;
    }
    if (outputs.size()>2){
     outputs[2] = outArray;
     }

    //Output 3 scores
     matlab::data::Array outArray2 =
             factory.createArray<double>({TraceScore.size(),1});
     for(int i=0;i<TraceScore.size();i++){
       outArray2[i] = TraceScore[i];
     }
     if (outputs.size()>3){
      outputs[3] = outArray2;
      }

      //output 4 alignmentmatrix
      matlab::data::Array outArray3 =
              factory.createArray<double>({alignmentscores.size(),alignmentscores[0].size()});
      for(int i=0;i<alignmentscores.size();i++){
        for(int j=0;j<alignmentscores[i].size();j++){
        outArray3[i][j] = alignmentscores[i][j];
      }
      }
      if (outputs.size()>4){
       outputs[4] = outArray3;
       }

  } //function
}; //class

//std::ostringstream stream;
// stream<<"hold: "<<holdpenalty<<" skip: "<<skippenalty<<std::endl;
// displayOnMATLAB(std::move(stream));
