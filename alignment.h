#ifndef ALIGNMENT_H_INCLUDED
#define ALIGNMENT_H_INCLUDED
#endif // ALIGNMENT_H_INCLUDED



#ifndef GRAPH_H_INCLUDED
#include "Graph.h"
#endif // GRAPH_H_INCLUDED

#ifndef FUNCTIONS_H_INCLUDED
#include "functions.h"
#endif // GRAPH_H_INCLUDED
#include <assert.h>
using std::vector;

#include <math.h>       /* log */
#include <algorithm>    /* reverse */

struct scoringStruct{

double stepscore;
double holdscore;
double skipscore;

int branch1;
int branch2;

int skipidx;
int holdidx;
vector<int> stepidx;

double maxscore;
int step;
};


class gg_alignment{
vector<vector<double>> lookuptable;
double Sigma;
bool Rounded = true;
double holdpenalty;
double skippenalty;

public:
gg_alignment(); //default constructor
gg_alignment(Graph Input1, Graph Input2);
gg_alignment(Graph Input1, Graph Input2, bool rounded);
gg_alignment(Graph Input1, Graph Input2, double sigma);
gg_alignment(Graph Input1, Graph Input2, double sigma, double pen1, double pen2);
gg_alignment(Graph Input1, Graph Input2, vector<vector<double>> lutable);

gg_alignment(Graph Input1, Graph Input2, vector<vector<double>> lutable,double pen1, double pen2);

void addParameters(vector<vector<double>> lutable, double pen1, double pen2);
void addParameters(double sigma, double pen1, double pen2);

vector<vector<double>> alignmentscores;
vector<vector<bool>> alignment_bool;
vector<vector<int>> step;
vector<vector<int>> branch1;
vector<vector<int>> branch2;

Graph Graph1;
Graph Graph2;

double maxEndScore;
Node endnode1;
Node endnode2;
int maxDirection;

vector<int> backTrace1;
vector<int> backTrace2;
vector<double> TraceScore;

void RunAlignment();
void alignment_initialize(int n, int m);
void FillInMatrix(scoringStruct scores, Node& node1, Node& node2);
double CalculateScore(Node& node1, Node& node2);
double CalculateScoreExact(Node& node1, Node& node2);
void AlignTwoNodes(Node& node1,Node& node2);

void GetBestOldScores(Node& node1, Node& node2, scoringStruct& scores);
double GetScoresforNode(Node &node1, Node &node2);
double Score(int val1, int val2);
double ScoreExact(double val1, double val2);

void StartBackTrace();
void BackTrace(Node& node1,Node& node2);
void DetermineNextStep(Node& node1, Node& node2, Node& nextnode1, Node& nextnode2);
};
